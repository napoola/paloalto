from netaddr import *
import pprint
import ipaddress
import re
import sys
import xlrd
campo_importato = ['10.25.73.200, 10.25.73.201, 10.25.73.202','10.25.73.200,10.25.73.201,10.25.73.202','10.25.73.200-10.25.73.202','10.25.73.220, 10.25.73.210, 10.25.73.215','10.25.73.200/24','10.25.73.200','SERVER1','SERVER1-10.25.73.200','SERVER1_10.25.73.200','10.25.73.200, 10.23.54.13','10.23.73.32/27, 10.23.73.64/26']
#campo_importato = ['10.25.73.200, 10.25.73.201, ...','10.25.73.200,10.25.73.201,10.25.73.202','10.25.73.200-10.25.73.202','10.25.73.220, 10.25.73.210, 10.25.73.215','10.25.73.200/24','10.25.73.200','SERVER1','SERVER1-10.25.73.200','SERVER1_10.25.73.200','10.25.73.200, 10.23.54.13','10.23.73.32/27, 10.23.73.64/26']

def stampa_2_righe_vuote():
    print ()
    print ()
    
def validate_ip(s):
    final_part_list=[]
    for i in s:
        first_part = re.findall( r'[0-9]+(?:\.[0-9]+){2}', i )
        final_part_list.append (first_part)
    print ('First Part:',final_part_list )
    
    result = all(element == final_part_list[0] for element in final_part_list)
    if (result):
        print("All the elements are Equal")
        return True
    else:
       print("All Elements are not equal")
       return False

# Verifica correttezza File XLS
for i in campo_importato:
    if '...' in i:
        print ('file excel errato')
        sys.exit()


pos=0
for i in campo_importato:
    # print array value
    print ('Posizione',pos)
    print ('Contenuto dell\'array:',i)

    #ip_list = re.findall( r'[0-9]+(?:\.[0-9]+){3}', i )

    #remove space from STRING
    i = i.replace(" ", "")

    # create a LIST from STRING
    if ',' in i:
        ip_list = i.strip('][').split(',')
    elif ', ' in i:
        ip_list = i.strip('][').split(', ')
    elif '-' in i:
        ip_list = i.strip('][').split('-') 
    elif '_' in i:
        ip_list = i.strip('][').split('_')
    else:
        ip_list = [i]
    
    # remove NOT-IP value from LIST
    for ip in ip_list:
        print (ip)
        if '/' in ip:
            try:
                ipaddress.ip_network(ip, strict=False)
            except (ValueError):
                # remove from list
                ip_list.remove (ip)
        else:
            try:
                ipaddress.ip_address(ip)
            except (ValueError):
                # remove from list
                ip_list.remove (ip)
    print ('Lista',ip_list)
    # Operazioni sui valori della lista utili successivamente
    if len(ip_list) >= 1:
        numero_ip_nella_lista = len(ip_list)
        max_valore_nella_lista = max(ip_list)
        min_valore_nella_lista = min(ip_list)
        #max_ip_nella_lista = ipaddress.ip_address(max_valore_nella_lista)
        #min_ip_nella_lista = ipaddress.ip_address(min_valore_nella_lista)
    else:
        numero_ip_nella_lista = 0

    # Elaborazione valore_finale
    if numero_ip_nella_lista == 0:
        valore_finale = ('Lista Vuota')
    elif numero_ip_nella_lista == 1:
        valore_finale = str(ip_list).strip('[]\'')
    elif numero_ip_nella_lista > 1:
        # check for first part of IP is the same
        if validate_ip(ip_list) == True:
            # Se i campi della lista sono subnet il range deve essere elaboeato
            if '/' in min_valore_nella_lista:
                subnet_min = IPNetwork(min_valore_nella_lista)
                min_valore_nella_lista= str(subnet_min.network)
            if '/' in max_valore_nella_lista:
                subnet_max = IPNetwork(max_valore_nella_lista)
                max_valore_nella_lista= str(subnet_max.broadcast)
            valore_finale = min_valore_nella_lista+'-'+max_valore_nella_lista
        elif validate_ip(ip_list) == False:
            # Da verificare a mano
            valore_finale = ('IP not in the same subnet')
        else:
            valore_finale = ('Errore nella funzione validate_ip')
    print ('*** VALORE FINALE:', valore_finale, '***')
    input("Press Enter to continue...")
    stampa_2_righe_vuote ()
    pos=pos+1
##
